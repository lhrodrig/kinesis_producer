import boto3
import hashlib
import pprint


def splitting_first_shard(streamName):
    client = boto3.client('kinesis')
    streamDes = client.describe_stream(
        StreamName=streamName,
    )
    new_hashkey = (int(streamDes['StreamDescription']['Shards'][0]['HashKeyRange']['EndingHashKey']) + int(streamDes['StreamDescription']['Shards'][0]['HashKeyRange']['StartingHashKey'])) // 2

    response = client.split_shard(
        StreamName=streamName,
        ShardToSplit=streamDes['StreamDescription']['Shards'][0]['ShardId'],
        NewStartingHashKey=str(new_hashkey)
    )
    print(response)

def describe_stream(streamName):
    client = boto3.client('kinesis')
    streamDes = client.describe_stream(
        StreamName=streamName,
    )

    pprint.pprint(streamDes)

def hash_calculator():
    

    shard1 = 0
    shard2 = 0

    for x in range(1, 15):
        hash = int(hashlib.md5(str(x).encode('utf-8')).hexdigest(), 16)
        if hash < 170141183460469231731687303715884105728:
            shard1 += 1
            print(hash, 'shard1', f" value of x : {x}")
        else:
            shard2 += 1
            print(hash, 'shard2',f" value of x : {x}")

    print('shard1:', shard1, 'shard2:', shard2)

def mergeShards(streamName):
    client = boto3.client('kinesis')
    response = client.merge_shards(
        StreamName=streamName,
        ShardToMerge='shardId-000000000001',
        AdjacentShardToMerge='shardId-000000000002'
    )
    pprint.pprint(response)


if __name__ == "__main__":
    mergeShards('sample-stream')