import glob
import os
import concurrent.futures
import boto3 
import time
import itertools
import uuid

NUMBER_OF_RECORDS_PER_REQUEST = 500
NUMBER_OF_THREADS = 3

#ROOT_DIRECTORY = 'C:/Users/Luis/Documents/tLogs'
ROOT_DIRECTORY = '/mnt/tLogs'

def split_list(alist, wanted_parts=1):
    length = len(alist)
    return [ alist[i*length // wanted_parts: (i+1)*length // wanted_parts] 
             for i in range(wanted_parts) ]

def put_records_kinesis(directory, streamName):  
    files_with_data = []
    failed_records = {}
    number_of_files_processed = 0
    client = boto3.client('kinesis')

    def put_records_and_save_failures(client, records_list, filename):
        response = client.put_records(
                Records = records_list,
                StreamName = streamName
            )
        time.sleep(1)
        if response['FailedRecordCount'] > 0:
            print(f'error on {filename}')
            failed_records[filename] = []
            for record in response['Records']:
                if 'ErrorCode' in record: 
                    failed_records[filename].append(record)     
                             
    def process_and_send(f_content):
        number_of_records_proccessed = 0
        records_list = []
        for line in f_content:
            
            encoded_registry = line.encode('utf-8')
            single_record = {
                'Data': encoded_registry,
                'PartitionKey': str(uuid.uuid1())
            }
            number_of_records_proccessed += 1
            records_list.append(single_record)                    
            if len(records_list) >= NUMBER_OF_RECORDS_PER_REQUEST :                        
                put_records_and_save_failures(client, records_list, filename)                               
                records_list = []

        # Process leftover records
        if len(records_list) > 0:            
            put_records_and_save_failures(client, records_list, filename)
        
        return number_of_records_proccessed

    

    for filename in glob.iglob(directory + '**/**', recursive=True):
        if os.path.isfile(filename): 
            with open(filename, 'r', encoding='utf-8') as f:
                number_of_files_processed += 1
                f_content = f.readlines()
                if len(f_content) != 0: files_with_data.append(f_content)

    separated_list = split_list(list(itertools.chain.from_iterable(files_with_data)), wanted_parts=NUMBER_OF_THREADS)
    with concurrent.futures.ThreadPoolExecutor(max_workers= NUMBER_OF_THREADS) as executor:
        results = executor.map(process_and_send, separated_list)
        for result in results:
            print(f'number of records processed on one thread: {result}')

    
    return failed_records


def send_records_kinesis(scanner_directory,single =True, server_name="bt-virginia-redirect-01"):
    list_servers_complete_path = []
    streamName = ''
    if scanner_directory == 'pbLogs':
        #Getting all posible directories for pbLogs, considering all the redirect servers
        streamName = 'sample-stream2'
        list_servers = os.listdir(ROOT_DIRECTORY)     
        for server in list_servers:
            complete_path = os.path.join(ROOT_DIRECTORY, server, scanner_directory, '2021/10')
            list_servers_complete_path.append(complete_path)
    else: 
        streamName = 'sample-stream'
        list_servers = os.listdir(ROOT_DIRECTORY)
        list_servers_path = []
        #Getting all posible directories for clickLogs, considering all the redirect servers
        for server in list_servers: 
            if server.startswith('bt-'):
                complete_path = os.path.join(ROOT_DIRECTORY, server, scanner_directory) # getting all redirect paths
                list_servers_path.append(complete_path)
        for elem_server_path in list_servers_path: # the clickLogs have am extra directoru level that have machine_ds
            machine_ids = os.listdir(elem_server_path)
            for id in machine_ids:
                complete_path_with_machine_id = os.path.join(elem_server_path, id, '2021/10')
                list_servers_complete_path.append(complete_path_with_machine_id)

    if single:
        single_server = list(filter(lambda x: server_name in x, list_servers_complete_path))
        if len(single_server) == 1:
            print(f'processing server: {single_server[0]}')
            put_records_kinesis(single_server[0], streamName)
        else:
            print(f"No single server found for {server_name}")
    else:
        for server in list_servers_complete_path:
            print(f'processing server: {server}')
            put_records_kinesis(server, streamName)



if __name__ == "__main__":
    # to process all the servers on the directory change single to False on both calls to the method
    #send_records_kinesis('clickLogs', single=False) 
    #send_records_kinesis('pbLogs', single=False)
    # This set-up process all redirect servers
    send_records_kinesis('clickLogs', single=False)
    print('Sleeping 20 ... to start processing pbLogs') 
    time.sleep(20)
    send_records_kinesis('pbLogs', single=False)
    
                    
        